const Discord = require('discord.js');
const client = new Discord.Client();
const format = /^(\w+( |$)){4}$/;
const fs = require('fs');

const linkDirectory = "./JSON Files/Links.JSON";
let linkRead = fs.readFileSync(linkDirectory);
let linkObject = JSON.parse(linkRead);

let commands = {
    "add": (newArray) => {
        /*
            *Function checks to see if the requester already has 
            *entries in the list specified. If not, it adds them
            *to the object. It also adds the specified value-key
            *pair to the object in the form of an array for the 
            *specified userID
        */
        if(!(newArray[4] in lists[newArray[1]])){
            lists[newArray[1]][newArray[4]] = [];
        }
        let actualLink = newArray[2];
        linkObject[newArray[4]].push({[actualLink] : newArray[3]});
        fs.writeFileSync(linkDirectory, JSON.stringify(linkObject));
        newArray[5].channel.send("[Furious scribbling] Aaaaaaaaaand noted!");
    
    },
    "list": (newArray) => {
        if(!(newArray[1] in lists)){
            newArray[4].channel.send("Sorry! That's not a list I'm keeping track of!");
            return;
        }

        if(newArray[1] == "links"){
            /*
                *Function gets the userID of the username specified,
                *makes sure it has recorded links, and then adds the links
                *to a stingified object.
            */
            let lookupID;
            try{
                lookupID = newArray[4].guild.members.find(m => m.user.username === newArray[2]).user.id;
            }
            catch(TypeError){
                newArray[4].channel.send("Sorry! That is not a user I recognize!");
                return;
            }
            
            if(!(lookupID in lists[newArray[1]])){ 
                newArray[4].channel.send("Sorry! But it looks like they haven't given me any social media links yet!");
                return;
            }
            else{
                var linkJoiner = '';
                let tempValue;
                for(let i = 0; i < linkObject[lookupID].length; i++){
                    tempValue = JSON.stringify(linkObject[lookupID][i]);
                    linkJoiner += tempValue.slice(1, tempValue.length -1) + '\n'; //Remove brackets
                }
                linkJoiner = linkJoiner.replace(/\"/g, '');
            }

            newArray[4].channel.send(`Here's the links you ordered: \n${linkJoiner}`);
        }
    },

    "help": (newArray) => {

    }
}

//Checks number of arguments before the functions are ever called
let formatCount = {
    "add": 6,
    "list": 5,
    "help": [3, 4]
}

let lists = {
    "links": linkObject
}

client.on('message', (receivedMessage) => {
    let messageArray = receivedMessage.content.slice(1).split(" ");
    messageArray.push(receivedMessage.author.id);
    messageArray.push(receivedMessage);
    if(receivedMessage.content.startsWith("!")){
        let letContinue = false;
        for(let i = 0; i < formatCount[messageArray[0]].length; i++){
            if(messageArray.length == formatCount[messageArray[0]][i]){
                letContinue = true;
            }
        }

        if(!(messageArray[0] in commands)){
            receivedMessage.channel.send("Sorry! That's not a command I reccognize!");
        }
        else if(!letContinue){
            receivedMessage.channel.send("Sorry! That command is not properly formatted!");
        }
        else{
            commands[messageArray[0]](messageArray);
        }
    }
})

bot_secret_token = "NjY4MjgxNjI2MjE1ODQxNzky.XiPBHA.S7bO99Mh01vmscbOrV931m1ptpc";

client.login(bot_secret_token)